<?php
$file = 'cities.txt';

$file_content = file_get_contents($file);
$content = explode(PHP_EOL, $file_content);
$count_list_content = count($content);

$content_array = array();
foreach ($content as $key => $val) {
	$expl_val = explode(' ', $val);
	if(count($expl_val) > 3){
		$first = array(implode(' ', array_slice($expl_val, 0, count($expl_val) - 2)));
		$check = array_slice($expl_val, -2, 2);
		$expl_val = array_merge($first, $check);
	}
	$content_array[] = $expl_val;
}
for($i=0; $i < $count_list_content; $i++){
	for($j=0; $j < $count_list_content; $j++){
		if($i != $j){
			$here = new POI($content_array[$i][1], $content_array[$i][2]);
			$there = new POI($content_array[$j][1], $content_array[$j][2]);
			$points[$i][$j] = intval($here->getDistanceInMetersTo($there) / 1000); // Kilometer
		}
	}
}
echo $content_array[0][0] . "\n";
for($i=0; $i < $count_list_content; $i++){
	if($i == 0){
		$prev_step = $i;
		$step = array_keys($points[$i], min($points[$i]))[0];
	} else{
		$prev_step = $step;
		$step = array_keys($points[$step], min($points[$step]))[0];

	}
	for($j=0; $j < $count_list_content; $j++){
		if(isset($points[$j][$prev_step])){
			unset($points[$j][$prev_step]);
		}
	}
	echo $content_array[$step][0] . "\n";
}

// Using Haversine Formula to calc distance
class POI {
    private $latitude;
    private $longitude;
    public function __construct($latitude, $longitude) {
        $this->latitude = deg2rad($latitude);
        $this->longitude = deg2rad($longitude);
    }
    public function getLatitude(){ return $this->latitude; }
    public function getLongitude(){ return $this->longitude; }
    public function getDistanceInMetersTo(POI $other) {
        $radiusOfEarth = 6371000;// Earth's radius in meters.
        $diffLatitude = $other->getLatitude() - $this->latitude;
        $diffLongitude = $other->getLongitude() - $this->longitude;
        $a = sin($diffLatitude / 2) * sin($diffLatitude / 2) +
            cos($this->latitude) * cos($other->getLatitude()) *
            sin($diffLongitude / 2) * sin($diffLongitude / 2);
        $c = 2 * asin(sqrt($a));
        $distance = $radiusOfEarth * $c;
        return $distance;
    }
}